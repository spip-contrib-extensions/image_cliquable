<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

// TRAITEMENT PIPELINE :
//****************************************************

function image_cliquable_insert_head_css($texte)
{
	$texte .= '<link rel="stylesheet" type="text/css" href="'.timestamp(find_in_path('images-rwd/image-map-rwd.css')).'" />'."\n";
	return $texte;
}

function image_cliquable_insert_head($texte)
{
	$texte .= '<script type="text/javascript" src="'.timestamp(find_in_path('images-rwd/image-map-rwd.js')).'"></script>'."\n";

	$texte .= '<script type="text/javascript" src="'.timestamp(find_in_path('maphilight/jquery.maphilight.min.js')).'"></script>'."\n";
	$texte .= '<script type="text/javascript">$(function() {$(".maphilight").maphilight(); });</script>'."\n";

	return $texte;
}


?>
