// Fonction pour recalculer les coordonnées des zones
function updateImageMap($img) {
  var map = $img.attr("usemap");
  if (!map) return;

  var $map = $(map);

  // Stocker la taille originale si pas déjà fait
  if (!$img.data("original-size")) {
    $img.data("original-size", {
      width: $img.get(0).naturalWidth,
      height: $img.get(0).naturalHeight,
    });
  }

  var originalSize = $img.data("original-size");
  var currentSize = {
    width: $img.width(),
    height: $img.height(),
  };

  // Calculer les ratios de redimensionnement
  var widthRatio = currentSize.width / originalSize.width;
  var heightRatio = currentSize.height / originalSize.height;

  // Mettre à jour les coordonnées de chaque zone
  $map.find("area").each(function () {
    var $area = $(this);

    // Sauvegarder les coordonnées originales si pas déjà fait
    if (!$area.data("original-coords")) {
      $area.data("original-coords", $area.attr("coords"));
    }

    // Calculer les nouvelles coordonnées
    var coords = $area.data("original-coords").split(",");
    var newCoords = coords.map(function (coord, i) {
      // Coordonnées paires (x) utilisent widthRatio, impaires (y) utilisent heightRatio
      return Math.round(coord * (i % 2 === 0 ? widthRatio : heightRatio));
    });

    // Appliquer les nouvelles coordonnées
    $area.attr("coords", newCoords.join(","));
  });

  // Réappliquer maphilight pour mettre à jour les surlignages
  if ($img.hasClass("maphilighted")) {
    $img.maphilight();
  }
}

// Appliquer aux images existantes
$("img[usemap]").each(function () {
  var $img = $(this);

  // Gérer le chargement initial de limage
  $img.on("load", function () {
    updateImageMap($img);
  });

  // Si limage est déjà chargée
  if (this.complete) {
    updateImageMap($img);
  }
});

// Gérer le redimensionnement de la fenêtre avec debounce
var resizeTimeout;
$(window).on("resize", function () {
  clearTimeout(resizeTimeout);
  resizeTimeout = setTimeout(function () {
    $("img[usemap]").each(function () {
      updateImageMap($(this));
    });
  }, 5); // Attendre 5ms après le dernier événement resize
});
